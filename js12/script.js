const body = document.querySelector('body');
const wrapper = document.createElement('div');
wrapper.setAttribute('class', 'wrapper');
wrapper.innerHTML = `
<div class='outputGetContainer'></div>
<button class="get_data_button">Make get request</button>
<div class='outputPostContainer'></div>
<button class="post_data_button">Make post request</button>
<form class="main">
<label for="fname">First name:</label>
<input type="text" id="fname" name="fname">
<label for="lname">Last name:</label>
<input type="text" id="lname" name="lname">
<label for="age">Age:</label>
<input type="text" id="age" name="age">
</form>
`
body.appendChild(wrapper);
const outputGetContainer = document.querySelector('.outputGetContainer');
const outputPostContainer = document.querySelector('.outputPostContainer');
const fnameInput = document.querySelector('#fname');
const lnameInput = document.querySelector('#lname');
const ageInput = document.querySelector('#age');

function get(url) {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.addEventListener("load", () => {
      if (request.status < 400)
        resolve(request.response);
      else
        reject(new Error("Request failed: " + request.statusText));
    });
    request.addEventListener("error", () => {
      reject(new Error("Network error"));
    });
    request.send();
  });
}

function post(url, requestuestBody) {
  return new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("POST", url, true);
    request.setRequestHeader('Content-Type', 'application/json');
    request.addEventListener("load", () => {
      if (request.status < 400)
        resolve(request.responseText);
      else
        reject(new Error("Request failed: " + request.statusText));
    });
    request.addEventListener("error", () => {
      reject(new Error("Network error"));
    });
    request.send(requestuestBody);
  });
}


document.querySelector('.get_data_button').addEventListener('click', () => {
  get("http://127.0.0.1:5501/data/01.data.json").then((text) => {
    outputGetContainer.innerHTML = text;
    document.querySelector('.main').setAttribute('style', 'display:block');
  }, (error) => {
    console.log(error);
  });
})

document.querySelector('.post_data_button').addEventListener('click', () => {

  let user = {
    firstName: `${fnameInput.value}`,
    lastName: `${lnameInput.value}`,
    age: `${ageInput.value}`,
  };

  let postData = "firstName=" + user.firstName + "&lastName=" + user.lastName + "&age=" + user.age;

  post("http://127.0.0.1:5501/js12/index.html", postData).then((text) => {
    console.log(text);
  }, (error) => {
    outputPostContainer.innerHTML = JSON.stringify(user);
    console.log(error);
  });
})









